from django.apps import AppConfig


class LandingpageConfig(AppConfig):
    name = 'landing_page'
