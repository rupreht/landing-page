from django.urls import path
from django.contrib.sitemaps.views import sitemap
from .sitemaps import StaticViewSitemap
from . import views

app_name = 'landing-page'

sitemaps = {
    'static': StaticViewSitemap,
}

urlpatterns = [
    path('', views.default, name='default'),
    path('sitemap.xml', sitemap, {'sitemaps': sitemaps},
        name='django.contrib.sitemaps.views.sitemap'),
    path('<str:slug>', views.by_slug, name='by-slug'),
]
