from django.contrib.sitemaps import Sitemap
from django.urls import reverse
from .models import LandingPage
from django.conf import settings

class StaticViewSitemap(Sitemap):
    priority = 0.5
    changefreq = 'daily'

    def items(self):
        return LandingPage.objects.filter(enable_pub=True)

    def lastmod(self, obj):
        return obj.date

    def location(self, item):
        return settings.DEMO_SITE_DEFAULT_URL + item.slug