from django import template
from django.template.defaultfilters import stringfilter

register = template.Library()

@register.filter
@stringfilter
def clean_tel(value):
    """
    """
    return ''.join(e for e in value if (e.isnumeric() or e == '+'))

register.filter('cleantel', clean_tel)