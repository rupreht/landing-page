"""project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.staticfiles import views
from django.urls import include, path, re_path

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^site/', include('landing_page.urls', namespace='landing_page')),
    url(r'^api/', include('api.urls', namespace='api')),
]

# Django's sitemap app.
if "django.contrib.sitemaps" in settings.INSTALLED_APPS:
    from django.contrib.sitemaps.views import sitemap
    from .sitemaps import StaticViewSitemap
    sitemaps = {"sitemaps": {"all": StaticViewSitemap}}
    urlpatterns += [
        url(r"^sitemap\.xml$", sitemap, sitemaps),
    ]

# Django's demo_site app.
if "demo_site" in settings.INSTALLED_APPS:
    urlpatterns += [
        url(r'', include('demo_site.urls', namespace='demo_site')),
    ]

# Django's debug_toolbar app.
if "debug_toolbar" in settings.INSTALLED_APPS:
    import debug_toolbar
    urlpatterns += [
        re_path('__debug__/', include(debug_toolbar.urls)),
    ]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += [
        re_path(r'^static/(?P<path>.*)$', views.serve),
    ]

