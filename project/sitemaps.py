from __future__ import unicode_literals

from django.contrib import sitemaps
from django.urls import reverse

from . import settings

class StaticViewSitemap(sitemaps.Sitemap):
    priority = 0.5
    changefreq = 'daily'

    def items(self):
        return ['site']

    def location(self, item):
        return reverse(item)