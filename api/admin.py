from django.contrib import admin
from .models import TelegramChats

class TelegramChatsAdmin(admin.ModelAdmin):
    list_display = ('description', 'app_id', )

admin.site.register(TelegramChats, TelegramChatsAdmin)
