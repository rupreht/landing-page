from django import forms

class PhoneForm(forms.Form):
    myphone = forms.CharField(label='My Phone', max_length=100)
