from django.conf import settings
from django.utils.translation import ugettext as _
import time
import telepot
import os
from telepot.loop import MessageLoop
from telepot.delegate import per_chat_id, create_open, pave_event_space
from api.models import TelegramChats

def tele_send_message(chat_id, text, parse_mode=None):
    telegramBot.sendMessage(
        chat_id,
        text,
        parse_mode,
        disable_web_page_preview=True)

class MemberSetter(telepot.helper.ChatHandler):
    def on_chat_message(self, msg):
        print(msg)
        if msg['text'] != '/start':
            if hasattr(settings, 'TELE_KEY'):
                if msg['text'] == settings.TELE_KEY:
                    self.sender.sendMessage(_('%s! I Add You Chat id:[%d] to App!') % (msg['chat']['first_name'], msg['chat']['id']))
                else:
                    self.sender.sendMessage(_('\'%s\'\n - Is Not a security HASH. No set as member you.') % msg['text'])
        else:
            # Add new user
            try:
                TelegramChats.objects.create(
                        app_id = 0,
                        chat_id = msg['chat']['id'],
                        description = msg['chat']['first_name']
                    )
            except:
                pass
            self.sender.sendMessage(_('Hi, %s!') % msg['chat']['first_name'])

if 'TELE_TOKEN' in os.environ and os.environ['TELE_TOKEN']:
    if hasattr(settings, 'TELE_PROXY'):
        telepot.api.set_proxy(settings.TELE_PROXY)

    telegramBot = telepot.DelegatorBot(settings.TELE_TOKEN, [
        pave_event_space()(
            per_chat_id(), create_open, MemberSetter, timeout=10
            ),
    ])
    MessageLoop(telegramBot).run_as_thread()
    print('Listening TelegramBot ...')
