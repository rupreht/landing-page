from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.core.mail import send_mail
from django.conf import settings

from .forms import PhoneForm
import os

import markdown

if "TELE_TOKEN" in os.environ:
    from api.telegram import tele_send_message
    from api.models import TelegramChats

def get_phone(request, subject, app_id):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = PhoneForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            message = 'Срочно звони мне: [' + form.cleaned_data['myphone']
            message += '](tel://' + form.cleaned_data['myphone'] + ')'

            try:
                send_mail(
                    subject,
                    message,
                    settings.EMAIL_HOST_USER,
                    settings.MESSAGE_RECIPIENT,
                    html_message=markdown.markdown(message),
                )
            except:
                print("Error send mail:", subject, message, sep=' ', end='\n', flush=True)
                pass

            # Django's Telegram app.
            try:
                if "TELE_TOKEN" in os.environ:
                    for telegram_chat_id in TelegramChats.objects.filter(app_id=app_id):
                        tele_send_message(telegram_chat_id.chat_id, '*' + subject + "*\n" + message, 'Markdown')
            except:
                print("Error send telegram:", subject, message, sep=' ', end='\n', flush=True)
                pass

            return HttpResponseRedirect('?thanks')

    # if a GET (or any other method) we'll create a blank form
    else:
        form = PhoneForm()

    return render(request, 'thanks.html', {'form': form})