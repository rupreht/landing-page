from django.urls import path

app_name = 'api'

from . import views

urlpatterns = [
    path('myphone/', views.get_phone, {'app_id': 1, 'subject': 'Новый телефон: Хочу на тренировку!'}),
]
