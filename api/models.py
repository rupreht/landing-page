from django.db import models
from django.utils.translation import ugettext as _

class TelegramChats(models.Model):
    """

    """
    app_id = models.IntegerField(_('App Id'), null=False, db_index=True, default='', help_text="API path for send form")
    chat_id = models.BigIntegerField(_('Chat Id User'), null=False, unique=True)
    description = models.TextField(_('Description'), null=True, blank=True)

    def __str__(self):
        return self.description

    class Meta:
        """

        """
        ordering = ['-app_id']
